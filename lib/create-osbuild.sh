#!/bin/bash

# Get OS data.
source /etc/os-release

set_vars() {
  DISTRO="${OS_PREFIX}${OS_VERSION}"
  # DISK_IMAGE is the target for the Makefile and the name of the resulted file.
  # Example: cs9-qemu-minimal-ostree.aarch64.img
  export DISK_IMAGE="${DISTRO}-${BUILD_TARGET}-${IMAGE_NAME}-${IMAGE_TYPE}.${ARCH}.${BUILD_FORMAT}"
  export UPGRADE_REPO_NAME="${DISTRO}-${BUILD_TARGET}-${IMAGE_NAME}-${IMAGE_TYPE}.${ARCH}.repo"
  # .raw file is needed for import as AMI in S3
  if [[ "${BUILD_FORMAT}" == "img" ]]; then
    BUILD_FORMAT="raw"
  fi
  IMAGE_FILE=${IMAGE_FILE:-"${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.${BUILD_FORMAT}"}
  export UPGRADE_REPO_DIR="${DOWNLOAD_DIRECTORY}/repo/${IMAGE_KEY}.repo"
  STREAM=${STREAM:-"upstream"}
  MPP_ARGS=""
  MANIFEST_DIR="sample-images/osbuild-manifests"
  CUSTOM_IMAGES_DIR="custom-images"
  DOWNSTREAM_DISTRO_TEMPLATE="files/downstream.yml"
  DOWNSTREAM_DISTRO_FILE="${MANIFEST_DIR}/distro/${DISTRO}.ipp.yml"
  OSBUILD_VERSION="${OSBUILD_VERSION:-}"
  IMAGE_SIZE="${IMAGE_SIZE:-"3900000000"}"
}

workaound_for_gpg() {
  #Workaround to solve GPG check failed
  update-crypto-policies --set LEGACY
}

install_pinned_osbuild() {
  # Workaround to solve latest version not found on copr or to pin
  # the osbuild packages versions for a release.
  # When using this workaround OSBUILD_VERSION could be set at the
  # CI/CD variables or at the config.yml
  local version="$1"

  dnf --nobest -y install "osbuild <= ${version}" \
                          "osbuild-tools <= ${version}" \
                          "osbuild-ostree <= ${version}"
}

install_dependencies() {
  # Add Osbuild upstream yum repo to install the latest version
  curl --output /etc/yum.repos.d/osbuild-centos-stream-9.repo \
    https://copr.fedorainfracloud.org/coprs/g/osbuild/osbuild/repo/centos-stream-9/group_osbuild-osbuild-centos-stream-9.repo

  # make copr a higher priority so that it's preferred over other
  # repos for osbuild
  PRIORITY=9
  echo "Setting copr osbuild repo priority to ${PRIORITY}"
  dnf config-manager --save --setopt="copr:copr.fedorainfracloud.org:group_osbuild:osbuild.priority=${PRIORITY}"

  dnf -y install make skopeo

  if  [[ -z "${OSBUILD_VERSION}" ]]; then
    dnf install -y osbuild osbuild-tools osbuild-ostree
  else
    install_pinned_osbuild "${OSBUILD_VERSION}"
  fi
}

# Create the distro file for the default mpp values for downstream
create_downstream_distro_file() {
  export DISTRO DOWNSTREAM_COMPOSE_URL UUID CI_REPOS_ENDPOINT
  # We want this to output variables without expansion
  # shellcheck disable=SC2016
  envsubst '${DISTRO},${DOWNSTREAM_COMPOSE_URL},${UUID},${CI_REPOS_ENDPOINT}' \
	  < "$DOWNSTREAM_DISTRO_TEMPLATE" \
	  > "$DOWNSTREAM_DISTRO_FILE"
}

# Add options to the osbuild to enable the SSH access to the image
enable_ssh() {
  OS_OPTIONS+=("ssh_permit_root_login=true")
  OS_OPTIONS+=("extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]")
}

add_ssh_key() {
  local ssh_public_key="${1}"
  MPP_ARGS="-D 'root_ssh_key=\"${ssh_public_key}\"'"
}

# and to enable image size specification, with a 3.9GB default size
set_image_size(){
  local image_size="${1}"
  OS_OPTIONS+=("image_size=\"${image_size}\"")
}

# Use the yum repo from the pipeline
override_yum_repo_with_product_build () {
  YUM_REPO_URL="${CI_REPOS_ENDPOINT}/${UUID}/repos/${DISTRO}"
  OS_OPTIONS+=("distro_baseurl=\"${YUM_REPO_URL}\"")
  OS_OPTIONS+=("distro_devel_repos=[]")
  OS_OPTIONS+=("distro_debug_repos=[]")
  OS_OPTIONS+=("distro_repos=[{\"id\":\"auto\",\"baseurl\":\"${YUM_REPO_URL}/${ARCH}/os/\"}]")
}

override_kernel_rpm() {
  OS_OPTIONS+=("kernel_version=\"${KERNEL_NVR}\"")
}

set_osbuild_options() {
  if [[ "${PACKAGE_SET}" == "${PRODUCT_BUILD_PREFIX}" ]]; then
    override_yum_repo_with_product_build
  fi

  if [[ "${TEST_IMAGE}" == "yes" ]]; then
    enable_ssh
    add_ssh_key "${SSH_KEY}"
  fi

  if [[ "${STREAM}" == "downstream" ]]; then
    create_downstream_distro_file
  fi

  if [[ -n "${KERNEL_NVR}" ]]; then
    override_kernel_rpm
  fi

  if [[ "${SET_IMAGE_SIZE}" =~ ^(True|yes)$ ]]; then
    set_image_size "${IMAGE_SIZE}"
  fi
}

build_target() {
  local target="${1}"

  make "${target}" \
       IMAGEDIR="${CUSTOM_IMAGES_DIR}" \
       DEFINES="${OS_OPTIONS[*]}" \
       MPP_ARGS="${MPP_ARGS}"
}

# Clean up
cleanup() {
  echo "[+] Cleaning up"
  make clean
}
