#!/bin/bash

install_git() {
  dnf install -y git
}

check_ssl() {
  if [[ "${SSL_VERIFY}" == "false" ]]; then
    export GIT_SSL_NO_VERIFY=true
  fi
}

clone_images_repo() {
  local VARIABLES_COUNT=$#
  if [ ${VARIABLES_COUNT} -eq 3 ]; then
    local repo_url="$1"
    local repo_dir="$2"
    local revision="$3"
  else
    echo -n "Expecting 3 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 1
  fi

  check_ssl
  # Repeat the clone until 5 times and waits 10 seconds in between if it is failed
  local n=0
  until [[ $n -ge 5 ]]
  do
    git clone "${repo_url}" "${repo_dir}" && break || {
      ((n++))
      sleep 10
    }
  done
  pushd "${repo_dir}" || exit 1
  if git ls-remote --tags origin "${revision}" | grep -q "${revision}"; then
    git checkout -b upstream "${revision}"
  else
    git checkout -b upstream "origin/${revision}"
  fi
  popd || exit 1
}
