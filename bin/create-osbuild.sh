#!/bin/bash

set -euxo pipefail

source lib/create-osbuild.sh

set_vars

install_dependencies

set_osbuild_options

cd "${MANIFEST_DIR}"

build_target "${DISK_IMAGE}"

echo "[+] Checking build repo needed? [${TEST_IMAGE}] [${IMAGE_NAME}]"
if [[ "${TEST_IMAGE}" == "yes" ]] &&
   [[ "${IMAGE_NAME}" == @(qa|qaqcom) ]] && [[ "${IMAGE_TYPE}" == "ostree" ]]; then
  #FIXME: workaround for setting the right ostree_ref
  # The default is $distro_name/$arch/$target-$name
  # But the $name part is not always right
  OS_OPTIONS+=("ostree_ref=\"${DISTRO}/${ARCH}/${BUILD_TARGET}-${IMAGE_NAME}\"")
  echo "[+] building remote repo"
  build_target "${UPGRADE_REPO_NAME}"
  echo "[+] Moving the generated remote repo"
  mkdir -p "$(dirname "$UPGRADE_REPO_DIR")"
  mv "$UPGRADE_REPO_NAME" "$UPGRADE_REPO_DIR"
fi

echo "[+] Moving the generated image"
mkdir -p "$(dirname "$IMAGE_FILE")"
mv "$DISK_IMAGE" "$IMAGE_FILE"

cleanup

echo "The final image is here: ${IMAGE_FILE}"
echo "Checksum for the image is in ${IMAGE_FILE%.*}.sha256"
