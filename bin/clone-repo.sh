#!/bin/bash

set -euxo pipefail

source lib/clone-repo.sh

OSBUILD_DIR="${OSBUILD_DIR:-sample-images}"
CUSTOM_IMAGES_DIR="${OSBUILD_DIR}/osbuild-manifests/custom-images"

echo "[+] Install Git"
install_git

echo "[+] Remove any previous mpp files"
rm -vfr "${OSBUILD_DIR}"

echo "[+] Clone the repository with the mpp files"
clone_images_repo "$REPO_URL" "$OSBUILD_DIR" "$REVISION"
echo "[+] Cloned"

echo "[+] Clone the repository with the custom images mpp files"
clone_images_repo "$CUSTOM_IMAGES_REPO" "$CUSTOM_IMAGES_DIR" "$CUSTOM_IMAGES_REF"
echo "[+] Cloned"
