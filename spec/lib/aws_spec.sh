Describe 'aws'
  Include ./lib/aws.sh

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End
    Mock pip3
      echo "pip3 $*"
    End

    It 'installs jq'
      When call install_dependencies
        The output should include "jq"
    End

    It 'installs awscli'
      When call install_dependencies
        The output should include "awscli"
    End
  End

  Describe 'configure_aws_settings'
    REGION="dummy-region"
    setup() {
      touch test-config
      export AWS_CONFIG_FILE="./test-config"
    }
    cleanup() {
      rm -rf ./test-config
    }
    BeforeAll setup
    AfterAll cleanup
    It 'configures the aws region '
      When call configure_aws_settings "${REGION}"
      The file "${AWS_CONFIG_FILE}" should be exist
      The output should be blank
      The contents of file "${AWS_CONFIG_FILE}" should include "region = ${REGION}"
      The contents of file "${AWS_CONFIG_FILE}" should include "output = json"
    End
  End

  Describe 'set_build_format'
    It 'set raw format if img'
      BUILD_FORMAT="img"

      When call set_build_format
      The output should equal "raw"
    End

    It 'leaves the same format if it is not img'
      BUILD_FORMAT="qcow2"

      When call set_build_format
      The output should equal "$BUILD_FORMAT"
    End
  End

  Describe 'compress_image'
    Mock xz
      :
    End
    IMAGE_FILE="fake_image_name"
    Parameters
      "True" "${IMAGE_FILE}"
      "yes" "${IMAGE_FILE}"
      "False" "${IMAGE_FILE}.xz"
      "no" "${IMAGE_FILE}.xz"
      "" "${IMAGE_FILE}.xz" # variable not defined or empty
    End

    It "IMPORT_IMAGE=$1"
      IMPORT_IMAGE="$1"

      When call compress_image
      The output should equal $2
    End
  End

  Describe 's3_cp'
    aws() {
      :;
    }
    It 'success with 3 argument variables'
      When call s3_cp "src_file" "destination" "aws_vars"
      The length of error should eq 0
      The status should be success
    End

    It 'fails with missing argument'
      ERROR_CODE=5
      When call s3_cp
      The error should include "Expecting 2 or 3 variables defined but got"
      The status should eq $ERROR_CODE
    End
  End

  Describe 'publish_ami_info'
    # Expected command: s3_cp "file_name" "bucket_path"
    s3_cp() {
      echo "upload: ${1} to s3://${2}"
    }
    remove() {
      rm -rf AMI_info*.json
    }
    AfterAll remove
    IMAGE_KEY="tesy-key"
    ARCH="dummy-arch"
    OS_PREFIX="test"
    OS_VERSION="0.1"
    BUILD_TYPE="test-build"
    IMAGE_NAME="test-image"
    IMAGE_TYPE="dummy"
    UUID="dummy-UUID"
    FILE_NAME="AMI_info_${IMAGE_NAME}_${ARCH}.json"
    It 'creates AMI_info json file and checks the content of the file'
      When call publish_ami_info "${IMAGE_ID}"
      The error should be blank
      The path ${FILE_NAME} should be file
      The contents of file  ${FILE_NAME} should include ${IMAGE_KEY}
      The contents of file  ${FILE_NAME} should include ${ARCH}
      The contents of file  ${FILE_NAME} should include ${OS_PREFIX}
      The contents of file  ${FILE_NAME} should include ${OS_VERSION}
      The contents of file  ${FILE_NAME} should include ${BUILD_TYPE}
      The contents of file  ${FILE_NAME} should include ${IMAGE_NAME}
      The contents of file  ${FILE_NAME} should include ${IMAGE_TYPE}
      The contents of file  ${FILE_NAME} should include ${UUID}
      The output should not be blank
    End

    It 'checks the stdout content'
      When call publish_ami_info "${IMAGE_ID}"
      The error should be blank
      The output should include ${IMAGE_KEY}
      The output should include ${ARCH}
      The output should include ${OS_PREFIX}
      The output should include ${OS_VERSION}
      The output should include ${BUILD_TYPE}
      The output should include ${IMAGE_NAME}
      The output should include ${IMAGE_TYPE}
      The output should include ${UUID}
    End

     It 'checks the upload of the file'
        When call publish_ami_info "${IMAGE_ID}"
        The error should be blank
        The output should include "upload: ${FILE_NAME} to s3:///dummy-UUID/sample-images"
     End

     It 'fails without given image id'
        ERROR_CODE=5
        When call publish_ami_info
        The error should include "Expecting at least 1 variables defined but got"
        The output should be blank
        The status should eq $ERROR_CODE
     End
  End

  Describe 'fix_arch_name'
    It 'set arm64 if arch is aarch64'
      ARCH="aarch64"

      When call fix_arch_name $ARCH
      The output should equal "arm64"
    End

    It 'leaves the same arch if it is not aarch64'
      ARCH="x86_64"

      When call fix_arch_name $ARCH
      The output should equal "$ARCH"
    End
  End

  Describe 'set_s3_updload_prefix'
    UUID="XXXX"
    Describe 'sample images should use sample-images dir'
      Parameters
        "True"
        "yes"
      End

      It "SAMPLE_IMAGE=$1"
        SAMPLE_IMAGE="$1"
        When call set_s3_upload_prefix
        The output should equal "XXXX/sample-images"
      End
    End

    Describe 'non sample images should use non-sample-images dir'
      Parameters
        "False"
        "no"
      End

      It "SAMPLE_IMAGE=$1"
        SAMPLE_IMAGE="$1"
        When call set_s3_upload_prefix
        The output should equal "XXXX/non-sample-images"
      End
    End

    Describe "uses pre-existing S3_UPLOAD_DIR variable"
      Parameters
        "True" "my-fake-images" "my-fake-images"
        "True" "" "sample-images"
        "" "my-fake-images" "my-fake-images"
        "" "" "non-sample-images"
      End

      It "usesi SAMPLE_IMAGES='$1' S3_UPLOAD_DIR='$2'"
        SAMPLE_IMAGE="$1"
        S3_UPLOAD_DIR="$2"
        When call set_s3_upload_prefix
        The output should equal "XXXX/${3}"
      End
    End
  End

  Describe 'do_not_import'
    Describe 'return 1 when True/yes and 0 when other values'
      Parameters
        "True" 1
        "yes" 1
        "False" 0
        "no" 0
	"" 0 # variable not defined or empty
      End

      It "IMPORT_IMAGE=$1"
        IMPORT_IMAGE="$1"
        When call do_not_import_image
        The status should equal $2
      End
    End
  End

  Describe 'create_cheksum_file'
    sha256sum () {
      filename="$1"
      if [ -f "${filename}" ]; then
        echo "907ac29fbd1d9f2a76f9e60938f2fbd02f9a752c4f633e326029185bc472856d ${filename}"
        return 0
      else
        echo "sha256sum: ${filename}: No such file or directory"
        return 1
      fi
    }
    IMAGE_NAME="fake-image-qcow2.xz"
    DIR_NAME=$(mktemp -d)
    cleanup_files() { rm -fr "${DIR_NAME}/*"; }
    cleanup() { rm -fr "$DIR_NAME"; }
    Before 'cleanup_files'
    AfterAll 'cleanup'

    It 'generates a checksum valid file'
      touch "${DIR_NAME}/${IMAGE_NAME}"
      When call create_checksum_file "$DIR_NAME" "$IMAGE_NAME"
        The path "${DIR_NAME}/${IMAGE_NAME}.sha256" should be file
        The contents of file "${DIR_NAME}/${IMAGE_NAME}.sha256" should include "907ac29fbd1d9f2a76f9e60938f2fbd02f9a752c4f633e326029185bc472856d $IMAGE_NAME"
    End

    It 'fails if the file does not exist'
      When call create_checksum_file "$DIR_NAME" "fake_file"
        The path "${DIR_NAME}/fake_file.sha256" should be file
        The contents of file "${DIR_NAME}/fake_file.sha256" should include "No such file or directory"
        The status should equal 1
    End
  End

  Describe 'import_snapshot'
    aws() {
      BUCKET="${2%%.*}"
      KEY="${3%%.*}"
      cat <<EOF
{
    "Description": "My server VM",
    "ImportTaskId": "import-snap-1234567890abcdef0",
    "SnapshotTaskDetail": {
        "Description": "My server VMDK",
        "DiskImageSize": "0.0",
        "Format": "VMDK",
        "Progress": "3",
        "Status": "active",
        "StatusMessage": "pending",
        "UserBucket": {
            "S3Bucket": "${BUCKET}",
            "S3Key": "${KEY}"
        }
    }
}
EOF
    }
    It 'returns a json when passed valid s3 bucket and s3 key arguments'
        When call import_snapshot "s3-bucket" "s3-key"
        The error should be blank
        The output should include "import-snap-1234567890abcdef0"
    End
    Describe 'failure with missing parameter'
      Parameters
        "" "s3-key"
        "s3-bucket" ""
      End
      It 'fails with missing parameter'
        ERROR_CODE=5
        When call import_snapshot $1 $2
        The error should include "Expecting 2 variables defined but got"
        The status should eq 5
      End
    End
  End

  Describe 'snapshots'
    Mock aws
      # Expected command:
      # aws ec2 describe-import-snapshot-tasks --import-task-ids "${import_task_id}"
      if [[ "$4" == "import-snap-000000" ]]; then
        cat <<EOF
{
  "ImportSnapshotTasks": [
    {
      "ImportTaskId": "import-snap-000000",
      "SnapshotTaskDetail": {
        "DiskImageSize": 8589934592.0,
        "Format": "RAW",
        "SnapshotId": "snap-000000",
        "Status": "completed",
        "UserBucket": {
          "S3Bucket": "auto-toolchain-ci-gitlab-workspaces",
          "S3Key": "fake_image.raw"
        }
      },
      "Tags": []
    }
  ]
}
EOF
      else
        echo "An error occurred (InvalidConversionTaskId.Malformed) when calling the DescribeImportSnapshotTasks operation: Tasks $4 not found"
      fi
    End
    IMPORT_TASK_ID="import-snap-000000"

    Describe 'describe_import_snapshot_tasks'
      It 'returns a json when passed a valid ImportTaskId'
        When call describe_import_snapshot_tasks "$IMPORT_TASK_ID"
        The output should include '"ImportTaskId": "import-snap-000000"'
      End
      It 'fail when passed a non valid ImportTaskId'
        When call describe_import_snapshot_tasks "import-snap-fake"
        The output should include "Tasks import-snap-fake not found"
        # It should fail but the function doesn't implement it
        #The status should be failure
      End
    End

    Describe 'import_status'
      It 'returns valid status when passed a valid ImportTaskId'
        When call import_status "$IMPORT_TASK_ID"
        The output should include 'completed'
      End
      It 'fail when passed a non valid ImportTaskId'
        When call import_status "import-snap-fake"
        The status should be failure
        The error should include "Invalid numeric literal"
      End
    End

    Describe 'snapshot_id'
      It 'returns valid SnapshotId when passed a valid ImportTaskId'
        When call snapshot_id "$IMPORT_TASK_ID"
        The output should include 'snap-000000'
      End
      It 'fail when passed a non valid ImportTaskId'
        When call snapshot_id "import-snap-fake"
        The status should be failure
        The error should include "Invalid numeric literal"
      End
    End

    Describe 'create_snapshot_tags'
      aws() {
        return 0
      }

      It 'success with correct tag arguments'
        When call create_snapshot_tags "snap-000000"
        The output should be blank
        The error should be blank
      End

      It 'fails with missing argument'
        ERROR_CODE=5
        When call create_snapshot_tags
        The error should include "Expecting 1 variable defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'copy_snapshot'
      aws() {
        cat << EOF
{
    "SnapshotId": "snap-0000"
}
EOF
      }
      It 'copy snapshot with given id from source to destination'
        When call copy_snapshot "snap-0000" "src" "dest"
        The output should include "snap-0000"
        The error should be blank
      End

      It 'fails with missing parameters'
        ERROR_CODE=5
        When call copy_snapshot "src" "dest"
        The error should include "Expecting 3 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'snapshot_state'
      aws() {
        cat <<EOF
{
    "Snapshots": [
        {
            "Description": "This is my snapshot",
            "Encrypted": false,
            "VolumeId": "vol-049df61146c4d7901",
            "State": "completed",
            "VolumeSize": 8,
            "StartTime": "2019-02-28T21:28:32.000Z",
            "Progress": "100%",
            "OwnerId": "012345678910",
            "SnapshotId": "snap-0000",
            "Tags": [
                {
                    "Key": "Stack",
                    "Value": "test"
                }
            ]
        }
    ]
}
EOF
      }
      It 'returns snapshot state with given snapshot id and region'
        When call snapshot_state "snap-0000:us-east-0"
        The error should be blank
        The output should eq "completed"
      End

      It 'fails with missing snapshot id'
        ERROR_CODE=5
        When call snapshot_state
        The error should include "Invalid argument, please use <snapshot_id>:<region> formant"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'register_image'
      aws() {
        cat <<EOF
{
    "ImageId": "ami-1a2b3c4d5eEXAMPLE"
}
EOF
      }

      It 'returns ImageId in json format when the image is succefully registered'
        When call register_image "name" "region" "arch" "root_device" "boot_mode" "snapshot_id"
        The output should eq "ami-1a2b3c4d5eEXAMPLE"
        The error should be blank
      End

      It 'fails with missing arguments'
        ERROR_CODE=5
        When call register_image
        The error should include "Expecting 6 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'create_image_tags'
      aws() {
        :;
      }

      It 'success with the correct input arguments'
        When call create_image_tags "id" "region" "uuid"
        The error should be blank
        The output should be blank
      End

      It 'fails with missing arguments'
        ERROR_CODE=5
        When call create_image_tags
        The error should include "Expecting at least 3 variables"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'grant_image_permissions'
      aws() {
        :;
      }
      It 'success with correct arguments'
        When call grant_image_permissions "image" "region" "user-id"
        The error should be blank
        The output should be blank
      End
      It 'fails with missing arguments'
        ERROR_CODE=5
        When call grant_image_permissions
        The error should include "Expecting 3 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End
  End

  Describe 'register-images'
   Mock aws
    cat <<EOF
{
    "Images": [
        {
            "VirtualizationType": "hvm",
            "Description": "Provided by Red Hat, Inc.",
            "PlatformDetails": "Red Hat Enterprise Linux",
            "EnaSupport": true,
            "Hypervisor": "xen",
            "State": "available",
            "SriovNetSupport": "simple",
            "ImageId": "ami-1234567890EXAMPLE",
            "UsageOperation": "RunInstances:0010",
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/sda1",
                    "Ebs": {
                        "SnapshotId": "snap-111222333444aaabb",
                        "DeleteOnTermination": true,
                        "VolumeType": "gp2",
                        "VolumeSize": 10,
                        "Encrypted": false
                    }
                }
            ],
            "Architecture": "x86_64",
            "ImageLocation": "123456789012/RHEL-8.0.0_HVM-20190618-x86_64-1-Hourly2-GP2",
            "RootDeviceType": "ebs",
            "OwnerId": "123456789012",
            "RootDeviceName": "/dev/sda1",
            "CreationDate": "2019-05-10T13:17:12.000Z",
            "Public": true,
            "ImageType": "machine",
            "Name": "RHEL-8.0.0_HVM-20190618-x86_64-1-Hourly2-GP2"
        }
    ]
}
EOF
    End
    IMAGE_ID="ami-1234567890EXAMPLE"

    Describe 'image_register_info'
      It 'returns Images json for requested image id'
        When call image_register_info "${IMAGE_ID}" "dummy-region"
        The output should include "\"ImageId\": \"${IMAGE_ID}\","
        The error should be blank
      End

      It 'fails with missing argument variables'
        ERROR_CODE=5
        When call image_register_info
        The error should include "Expecting 2 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'image_register_status'
      It 'returns the image state with given image_id:region'
        When call image_register_status "image:dummy-region"
        The error should be blank
        The output should eq "available"
      End

      It 'fails with wrong argument'
        ERROR_CODE=5
        When call image_register_status "wrong-argument"
        The error should include "Invalid argument"
        The status should eq $ERROR_CODE
      End
    End
  End

  Describe 'task_progress'
    It 'fails with missing arguments'
      ERROR_CODE=5
      When call task_progress "echo" "dummy-id"
      The error should include 'Expecting 3 or 4 variables defined'
      The status should eq $ERROR_CODE
    End
  End

  Describe 'delete_raw_images'
    BUCKET="my-bucket"
    UPLOAD_PREFIX="prefix"
    IMAGE_KEY="key"
    # Expected command: aws s3 rm "s3://bucket/prefix/file.extension"
    aws() {
      URI="${3}"
      echo "delete: ${URI}"
    }

    Context "remove all the rigth files"
      Parameters
        "raw"
        "raw.sha256"
        "json"
      End

      It "removes the image's $1 file from the s3 bucket"
        When call delete_raw_images "${BUCKET}" "${UPLOAD_PREFIX}" "${IMAGE_KEY}"
        The output should include "delete: s3://${BUCKET}/${UPLOAD_PREFIX}/${IMAGE_KEY}.${1}"
        The error should be blank
      End
    End

    It 'fails with missing arguments'
      ERROR_CODE=5
      When call delete_raw_images
      The error should include "Expecting 3 variables defined but got"
      The status should eq $ERROR_CODE
    End
  End
End
