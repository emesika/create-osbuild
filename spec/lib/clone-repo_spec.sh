Describe 'clone-repo'
  Include ./lib/clone-repo.sh

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End

    It 'installs git'
      When call install_git
        The output should include "git"
    End
  End

  Describe 'check_ssl'
    BeforeEach 'unset GIT_SSL_NO_VERIFY'

    It 'disable ssl to git if SSL_VERIFY is false'
      SSL_VERIFY="false"
      When call check_ssl
        The variable GIT_SSL_NO_VERIFY should equal "true"
    End

    It 'do not disable ssl to git if SSL_VERIFY is true'
      SSL_VERIFY="true"
      When call check_ssl
        The variable GIT_SSL_NO_VERIFY should not be defined
    End
  End

  Describe 'clone_images_repo'
    REPO_URL="https://my-org.org/repo.git"
    REPO_DIR="osbuild-manifests"
    REVISION="fake-branch"
    clean_environment() {
      unset GIT_SSL_NO_VERIFY
      rm -fr "${REPO_DIR}"
    }
    git() {
      if [[ "$1" == "clone" ]]; then
        mkdir -p "${3}/.git/"
	touch "${3}/.git/config"
      elif [[ "$1" == "ls-remote" ]]; then
        return 1
      fi
      echo "git $*"
    }
    pushd() {
      if [[ ! -d "$1" ]]; then
        echo "pushd: $1: No such file or directory" >&2
	return 1
      fi
      echo "$1 $PWD"
    }
    popd() {
      echo "$PWD"
    }
    BeforeEach 'clean_environment'

    Context "Use SSL_VERIFY"
      Parameters
        "false" "be defined" "equal true"
        "true" "not be defined" "not be defined"
      End

      It "disable ssl to git if SSL_VERIFY is $1"
        SSL_VERIFY="$1"
        When call clone_images_repo "$REPO_URL" "$REPO_DIR" "$REVISION"
          The variable GIT_SSL_NO_VERIFY should $2
          The variable GIT_SSL_NO_VERIFY should $3
          The line 1 of stdout should equal "git clone https://my-org.org/repo.git osbuild-manifests"
          The line 3 of stdout should equal "git checkout -b upstream origin/fake-branch"
      End
    End
    Context "Parameters"
      It 'fails with no parameters'
        When call clone_images_repo
	  The error should include "Expecting 3 variables defined but got"
          The status should be failure
	  The path "${REPO_DIR}/.git/config" should not be file
      End
      It 'fails with less than 3 parameters'
        When call clone_images_repo "$REPO_URL" "$REPO_DIR"
	  The error should include "Expecting 3 variables defined but got"
          The status should be failure
	  The path "${REPO_DIR}/.git/config" should not be file
      End
      It 'clone the repo with 3 parameters'
        When call clone_images_repo "$REPO_URL" "$REPO_DIR" "$REVISION"
	  The error should be blank
          The line 1 of stdout should equal "git clone https://my-org.org/repo.git osbuild-manifests"
          The line 3 of stdout should equal "git checkout -b upstream origin/fake-branch"
          The status should be success
	  The path "${REPO_DIR}/.git/config" should be file
      End
    End
  End
End
