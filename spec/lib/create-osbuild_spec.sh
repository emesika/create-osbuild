Describe 'create-osbuild'
  Include ./lib/create-osbuild.sh
  SSH_KEY="ssh-rsa my-public-key"
  CI_REPOS_ENDPOINT="http://my.fake.repo"
  UUID="XXX"
  ARCH="ppc64"
  DISTRO="cs9"
  DOWNSTREAM_COMPOSE_URL="https://my-fake.repo/rhel9"
  DOWNSTREAM_DISTRO_TEMPLATE="files/downstream.yml"
  DOWNSTREAM_DISTRO_FILE="rhel9.yml"

  cleanup_distro_file() { rm -f "$DOWNSTREAM_DISTRO_FILE"; }

  Describe 'set variables'
    OS_PREFIX="cs"
    OS_VERSION="9"
    BUILD_TARGET="qemu"
    IMAGE_NAME="minimal"
    IMAGE_TYPE="ostree"
    ARCH="aarch64"
    BUILD_FORMAT="img"

    It 'sets the proper DISTRO variable'
      When call set_vars
      The value "${DISTRO}" should eq "cs9"
      The value "${DISTRO}" should not eq "cs8"
      The value "${DISTRO}" should not eq "rhel9"
    End

    It 'sets the proper DISK_IMAGE'
      When call set_vars
      The value "${DISK_IMAGE}" should eq "cs9-qemu-minimal-ostree.aarch64.img"
    End

    It 'sets the proper format for raw images'
      When call set_vars
      The value "${DISK_IMAGE}" should end with ".img"
      The value "${IMAGE_FILE}" should end with ".raw"
      The value "${IMAGE_FILE}" should not end with ".qcow2"
    End

    It 'sets the proper format for qcow2 images'
      BUILD_FORMAT=qcow2
      When call set_vars
      The value "${DISK_IMAGE}" should end with ".qcow2"
      The value "${IMAGE_FILE}" should end with ".qcow2"
      The value "${IMAGE_FILE}" should not end with ".raw"
    End

    It 'sets a template for the distro file that exist'
      When call set_vars
      The path "${DOWNSTREAM_DISTRO_TEMPLATE}" should be file
    End
  End

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End
    Mock curl
      :
    End

    It 'installs osbuild dependencies'
      When call install_dependencies
        The output should include "osbuild"
        The output should include "osbuild-tools"
        The output should include "osbuild-ostree"
    End

    It 'installs make'
      When call install_dependencies
        The output should include "make"
    End

    It 'installs skopeo for the container image'
      When call install_dependencies
        The output should include "skopeo"
    End
  End

  Describe 'enable SSH'
    It 'adds ssh packages and enable ssh in the image'
      When call enable_ssh
      The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login=true"
      The value "${OS_OPTIONS[*]}" should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
    End
  End

  Describe 'add SSH key'
    It 'adds specific public ssh key'
      When call add_ssh_key "$SSH_KEY"
      The value "$MPP_ARGS" should equal "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
    End
  End

  Describe 'set image size'
    It 'sets a smaller default image size'
      When call set_vars
      The value "${IMAGE_SIZE}" should eq "3900000000"
    End

    It 'passes a value for image size'
      When call set_image_size 4000000000
      The value "${OS_OPTIONS[*]}" should include "image_size=\"4000000000\""
    End
  End

  Describe 'use product build at yum repo'
    It 'sets product build as distro_baseurl'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
    End

    It 'clears all the predefined repos (devel and debug)'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should include "distro_devel_repos=[]"
      The value "${OS_OPTIONS[*]}" should include "distro_debug_repos=[]"
    End

    It 'sets new repo "auto" that point to the product build plus the current arch'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should include "distro_repos=[{\"id\":\"auto\",\"baseurl\":\"http://my.fake.repo/XXX/repos/cs9/ppc64/os/\"}]"
    End

    It 'should not set the CS9 repo as distro_baseurl'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should not include "distro_baseurl=\"http://mirror.stream.centos.org/9-stream\""
    End
  End

  Describe 'add work-in-progress kernel package'
    It 'adds specific kernel version'
      KERNEL_NVR="5.14.0-101.62.ER1.el9s"
      When call override_kernel_rpm
      The value "${OS_OPTIONS[*]}" should include "kernel_version=\"5.14.0-101.62.ER1.el9s\""
    End
  End

  Describe 'set_osbuild_options'
    It 'should support bool formats yes'
      TEST_IMAGE="yes"
      PACKAGE_SET="cs9"
      PRODUCT_BUILD_PREFIX="autosd9"

      When call set_osbuild_options
      The status should be success
      The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login=true"
    End

    It 'should support bool formats no'
      TEST_IMAGE="no"
      PACKAGE_SET="cs9"
      PRODUCT_BUILD_PREFIX="autosd9"

      When call set_osbuild_options
      The status should be success
      The value "${OS_OPTIONS[*]}" should not include "ssh_permit_root_login=true"
    End

    Describe 'set options for building an Upstream image with no Product build and not for testing'
      TEST_IMAGE="no"
      PACKAGE_SET="cs9"
      PRODUCT_BUILD_PREFIX="autosd9"

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "ssh_permit_root_login=true"
        The value "${OS_OPTIONS[*]}" should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "$MPP_ARGS" should be blank
      End

      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
    End

    Describe 'set options for building an Upstream image with no Product build but for testing'
      TEST_IMAGE=yes
      PACKAGE_SET=cs99
      PRODUCT_BUILD_PREFIX=autosd9

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login=true"
        The value "${OS_OPTIONS[*]}" should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "$MPP_ARGS" should equal "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End

      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
    End

    Describe 'set options for building an Upstream image with Product build but not for testing'
      TEST_IMAGE=no
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "ssh_permit_root_login=true"
        The value "${OS_OPTIONS[*]}" should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "$MPP_ARGS" should be blank
      End

      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
    End

    Describe 'set options for building an Upstream image with Product build and for testing'
      TEST_IMAGE=yes
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login=true"
        The value "${OS_OPTIONS[*]}" should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "$MPP_ARGS" should equal "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End

      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
    End

    Describe 'set options for building a Downstream image with Product build'
      STREAM="downstream"
      DISTRO="rhel9"
      PACKAGE_SET="rhivos9"
      PRODUCT_BUILD_PREFIX="rhivos9"
      BeforeEach 'cleanup_distro_file'
      AfterEach 'cleanup_distro_file'

      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/rhel9\""
      End
    End

    Describe 'set options for building a Downstream image with Product build'
      STREAM="downstream"
      DISTRO="rhel9"
      PACKAGE_SET="rhel9"
      PRODUCT_BUILD_PREFIX="rhivos9"
      BeforeEach 'cleanup_distro_file'
      AfterEach 'cleanup_distro_file'

      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/rhel9\""
        The contents of file "${DOWNSTREAM_DISTRO_FILE}" should include "distro_baseurl: https://my-fake.repo/rhel9"
      End
    End

    Describe 'set options for specific IMAGE_SIZE if SET_IMAGE_SIZE is set'
      Parameters
        "True" "" "image_size=\"3900000000\""
        "yes" "8888888888" "image_size=\"8888888888\""
        "False" "" ""
        "no" "8888888888" ""
      End

      It "has defined SET_IMAGE_SIZE as \"$1\" and IMAGE_SIZE as \"$2\""
        SET_IMAGE_SIZE="$1"
	IMAGE_SIZE="$2"
	set_vars
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "$3"
      End
    End
  End

  Describe 'build_target'
    Mock make
      echo "make $*"
    End

    Describe 'build sample image (not for testing or for rpi4)'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=no
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'uses the product build as distro_baseurl'
        When call build_target
        The output should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has no the ssh enabled'
        When call build_target
        The output should not include "ssh_permit_root_login=true"
        The output should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'has not the Testing Farm public ssh key'
        When call build_target
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
    End

    Describe 'build sample image (for rpi4)'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=no
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'uses the product build as distro_baseurl'
        When call build_target
        The output should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has no the ssh enabled'
        When call build_target
        The output should not include "ssh_permit_root_login=true"
        The output should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'has not the Testing Farm public ssh key'
        When call build_target
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
    End

    Describe 'build sample image (for testing)'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=yes
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'uses the product build as distro_baseurl'
        When call build_target
        The output should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has the ssh enabled'
        When call build_target
        The output should include "ssh_permit_root_login=true"
        The output should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'has the Testing Farm public ssh key'
        When call build_target
        The output should include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End

      It 'should not use the wrong ssh key'
        When call build_target
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-wrong-key\"'"
      End
    End

    Describe 'build non sample image (not for testing)'
      SAMPLE_IMAGE=False
      TEST_IMAGE=no
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'does not use the product build as distro_baseurl'
        When call build_target
        The output should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has no the ssh enabled'
        When call build_target
        The output should not include "ssh_permit_root_login=true"
        The output should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'has not the Testing Farm public ssh key'
        When call build_target
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
    End

    Describe 'build non sample image (for testing)'
      SAMPLE_IMAGE=False
      TEST_IMAGE=yes
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'does not use the product build as distro_baseurl'
        When call build_target
        The output should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has the ssh enabled'
        When call build_target
        The output should include "ssh_permit_root_login=true"
        The output should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'adds the Testing Farm public ssh key'
        When call build_target
        The output should include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End

      It 'should not use the wrong ssh key'
        When call build_target
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-wrong-key\"'"
      End
    End

    Describe 'build the QA image is a sample image for testing, but uses CS9'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=yes
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'does not use the product build as distro_baseurl'
        When call build_target
        The output should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has the ssh enabled'
        When call build_target
        The output should include "ssh_permit_root_login=true"
        The output should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'adds the Testing Farm public ssh key'
        When call build_target
        The output should include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End

      It 'should not use the wrong ssh key'
        When call build_target
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-wrong-key\"'"
      End
    End
  End

  Describe 'create downstream distro file'
    DISTRO="rhel9"
    BeforeEach 'cleanup_distro_file'
    AfterEach 'cleanup_distro_file'

    It 'downstream distro file has the right prefix'
      When call create_downstream_distro_file
      The contents of file "${DOWNSTREAM_DISTRO_FILE}" should include "distro_name: rhel9"
    End

    It 'downstream distro file has the right baseurl'
      When call create_downstream_distro_file
      The contents of file "${DOWNSTREAM_DISTRO_FILE}" should include "distro_baseurl: https://my-fake.repo/rhel9"
    End
  End

End
